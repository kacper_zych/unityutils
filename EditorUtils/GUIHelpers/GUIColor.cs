﻿using System;
using UnityEngine;

namespace UnityUtilsEditor.GUIHelpers
{
    public class GUIColor : IDisposable
    {
        readonly Color defaultColor;

        public GUIColor(Color color)
        {
            defaultColor = GUI.color;
            GUI.color = color;
        }

        public void Dispose()
        {
            GUI.color = defaultColor;
        }
    }
}