﻿#if UNITY_EDITOR
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace UnityUtilsEditor.BuildHelper
{
    [UsedImplicitly]
    public class LastBuildPaths : IPostprocessBuildWithReport
    {
        public static readonly LastBuildPath androidLastBuildPath = new LastBuildPath(BuildTarget.Android);
        public static readonly LastBuildPath iOSLastBuildPath = new LastBuildPath(BuildTarget.iOS);
        
        public int callbackOrder { get { return 0; } }

        public void OnPostprocessBuild(BuildReport report)
        {
            switch (report.summary.platform) 
            {
                case BuildTarget.Android:
                    androidLastBuildPath.Path = report.summary.outputPath;
                    break;
                case BuildTarget.iOS:
                    iOSLastBuildPath.Path = report.summary.outputPath;
                    break;
            }
        }

        public class LastBuildPath
        {
            readonly BuildTarget target;

            public LastBuildPath(BuildTarget target)
            {
                this.target = target;
            }

            string PrefKey => $"{UnityEngine.Application.productName}lastBuildPath{target}";

            public string Path
            {
                get => EditorPrefs.GetString(PrefKey, "");
                set => EditorPrefs.SetString(PrefKey, value);
            }
        }
    }

}
#endif