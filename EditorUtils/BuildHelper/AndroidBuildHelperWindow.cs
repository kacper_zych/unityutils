﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace UnityUtilsEditor.BuildHelper
{
    public class AndroidBuildHelperWindow : EditorWindow
    {
        [MenuItem("Window/Unity Utils/Android Build Helper")]
        static void Init()
        {
            var window = (AndroidBuildHelperWindow)GetWindow(typeof(AndroidBuildHelperWindow), false, "Android Build Helper");
            window.Show();
        }
        
        void OnGUI()
        {
            var lastBuildPath = LastBuildPaths.androidLastBuildPath.Path;
            GUILayout.Label("Last build path: " + lastBuildPath);
            using (new EditorGUI.DisabledGroupScope(string.IsNullOrEmpty(lastBuildPath)))
            {
                if (GUILayout.Button("Install last build"))
                    AdbRequest.InstallToDevice(lastBuildPath, null);

                if (GUILayout.Button("Open last build path"))
                    EditorUtility.RevealInFinder(lastBuildPath);
            }

            if (GUILayout.Button("Run on device"))
                AdbRequest.RunOnDevice(Application.identifier);

            if (GUILayout.Button("Uninstall app"))
                AdbRequest.Uninstall(Application.identifier, null, null);

            if (GUILayout.Button("Clear cache"))
                AdbRequest.ClearCache(Application.identifier, null, null);

            DropAreaGUI ();
        }

        static void DropAreaGUI()
        {
            Event evt = Event.current;
            Rect dropArea = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
            GUI.Box(dropArea, "Drop APK file to install");

            switch (evt.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    OnDrag(dropArea, evt);
                    return;
            }
        }

        static void OnDrag(Rect dropArea, Event evt)
        {
            if (!dropArea.Contains(evt.mousePosition))
                return;

            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

            if (evt.type == EventType.DragPerform)
            {
                DragAndDrop.AcceptDrag();
                OnDrop();
            }
        }

        static void OnDrop()
        {
            if (DragAndDrop.paths.Length > 1)
                Debug.LogError("Drop one file");
            else
            {
                string path = DragAndDrop.paths[0];
                if (path.EndsWith(".apk"))
                {
                    AdbRequest.InstallToDevice(path, null);
                }
                else
                    Debug.LogError("Drop apk file");
            }
        }
    }
}
#endif