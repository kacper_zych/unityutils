﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityUtilsEditor.GUIHelpers;

namespace UnityUtilsEditor.ScenesSwitcher
{
    public class ScenesInspector : EditorWindow
    {
        const string WINDOW_NAME = "Scenes Inspector";
        const string QUICKPLAY_TEXT = "Play from First Scene";

        [MenuItem("Window/Unity Utils/"+WINDOW_NAME)]
        static void CreateWindow()
        {
            var window = (ScenesInspector)GetWindow(typeof(ScenesInspector), false, WINDOW_NAME);
            window.Show();
        }

        void OnGUI()
        {
            DrawIncludedScenes();
            GUILayout.Space(15);
            DrawPlayButton();
        }

        void DrawIncludedScenes()
        {
            foreach (var scene in EditorBuildSettings.scenes)
            {
                using (new GUIColor(HEditorScenesUtils.IsSceneLoaded(scene.path) ? Color.red : GUI.color))
                {
                    if (GUILayout.Button(GetNameFromScenePath(scene.path)))
                    {
                        HEditorScenesUtils.StartScene(scene.path);
                    }
                }
            }
        }

        string GetNameFromScenePath(string path)
        {
            return path.Split('/').Last();
        }

        void DrawPlayButton()
        {
            using (new GUIColor(Color.green))
            {
                if (GUILayout.Button(QUICKPLAY_TEXT, GUILayout.Height(40f)))
                {
                    HEditorScenesUtils.StartFirstScene();
                }
            }
        }
    }
}