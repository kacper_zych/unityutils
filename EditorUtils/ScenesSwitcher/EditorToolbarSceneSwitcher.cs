﻿#if !HFRAMEWORK_EDITOR_TOPBAR_OFF

using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityToolbarExtender;
using UnityUtilsEditor.GUIHelpers;

namespace UnityUtilsEditor.ScenesSwitcher
{
    [InitializeOnLoad]
    internal static class EditorToolbarSceneSwitcher
    {
        static EditorToolbarSceneSwitcher()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
        }

        static void OnToolbarGUI()
        {
            using (new GUIEnabled(EditorApplication.isPlaying == false))
            {
                ShowPlayButton();
                GUILayout.FlexibleSpace();
                ShowMenu();
            }
        }

        static void ShowPlayButton()
        {
            var tex = EditorGUIUtility.IconContent(@"PlayButton").image;
            
            using (new GUIColor(new Color(0.34f, 1f, 0.64f)))
            {
                var content = new GUIContent(tex, "Start First scene");
                var guiStyle = ToolbarStyles.Command;

                var rect = ToolbarStyles.GetThickArea(GUILayoutUtility.GetRect(content, guiStyle));
                if (GUI.Button(rect, content, guiStyle))
                {
                    HEditorScenesUtils.StartFirstScene();
                }
            }
        }

        static void ShowMenu()
        {
            var content = new GUIContent("Build Scenes ");
            var guiStyle = ToolbarStyles.DropDown;
            var rect = ToolbarStyles.GetThinArea(GUILayoutUtility.GetRect(content, guiStyle));

            if (GUI.Button(rect, content, guiStyle))
            {
                GenericMenu menu = new GenericMenu();

                foreach (var scene in EditorBuildSettings.scenes)
                {
                    bool isLoaded = HEditorScenesUtils.IsSceneLoaded(scene.path); 
                    menu.AddItem(new GUIContent(GetNameFromScenePath(scene.path)), isLoaded, StartScene, scene.path);
                }

                menu.ShowAsContext();
            }
        }

        static void StartScene(object pathText)
        {
            HEditorScenesUtils.StartScene(pathText as string);
        }

        static string GetNameFromScenePath(string path)
        {
            return path.Split('/').Last();
        }
    }
}

#endif