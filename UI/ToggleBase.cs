﻿using UnityEngine;
using UnityEngine.UI;

namespace UnityUtils.UI 
{
    [RequireComponent(typeof(Toggle))]
    public abstract class ToggleBase : MonoBehaviour
    {
        Toggle toggle;
        protected Toggle Toggle => toggle != null ? toggle : toggle = GetComponent<Toggle>();

        void OnEnable()
        {
            Toggle.onValueChanged.AddListener(ClickHandler);
        }

        void OnDisable()
        {
            Toggle.onValueChanged.RemoveListener(ClickHandler);
        }

        protected abstract void ClickHandler(bool value);
        
        public virtual void Setup(bool value)
        {
            Toggle.isOn = value;
        }
    }
}