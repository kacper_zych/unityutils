using UnityEngine;
using UnityEngine.UI;
using UnityUtils.Extensions;

namespace UnityUtils.UI
{
    [RequireComponent(typeof(Button))]
    public abstract class ButtonBase : MonoBehaviour
    {
        Button button;
        protected Button MyButton => button.IsNotNull() ? button : button = GetComponent<Button>();

        protected virtual void Awake()
        {
            MyButton.onClick.AddListener(OnClick);
        }

        protected virtual void OnDestroy()
        {
            MyButton.onClick.RemoveListener(OnClick);
        }

        protected abstract void OnClick();
    }
}