using TMPro;
using UnityEngine;
using UnityUtils.Extensions;

namespace UnityUtils.UI 
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextBase : MonoBehaviour
    {
        TMP_Text myText;
        protected TMP_Text MyText => myText.IsNotNull() ? myText : myText = GetComponent<TMP_Text>();
    }
}