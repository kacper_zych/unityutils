﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityUtils.Extensions;

namespace UnityUtils.UI
{
    public abstract class SwitchBase : ToggleBase, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] RectTransform handle = default;
        float handleMoveDuration = .2f;
        [SerializeField] bool inverted = default;

        Coroutine inputCoroutine;

        protected virtual void Awake()
        {
            if (handle == null)
                handle = transform.FindDeep("Handle") as RectTransform;
            
            handle.anchorMin = new Vector2(.5f, handle.anchorMin.y);
            handle.anchorMax = new Vector2(.5f, handle.anchorMax.y);
        }

        public override void Setup(bool value)
        {
            base.Setup(value);
            MoveToggle(value, false);
        }

        protected override void ClickHandler(bool value)
        {
            MoveToggle(value, true);
            SwitchHandler(value);
        }

        protected abstract void SwitchHandler(bool value);

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            handle.DOScale(1.3f, .2f)
                .SetUpdate(true);
            StartInputCoroutine();
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            handle.DOScale(1f, .2f)
                .SetUpdate(true);
            StopInputCoroutine();
        }

        void StartInputCoroutine()
        {
            StopInputCoroutine();
            inputCoroutine = StartCoroutine(ProcessInput());
        }

        bool isInputInsideToggle;

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            isInputInsideToggle = true;
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            isInputInsideToggle = false;
        }
        
        IEnumerator ProcessInput()
        {
            var toggleTransform = transform;
            while (HInput.IsInputPressed())
            {
                if (!isInputInsideToggle && 
                    HInput.GetInputPosition().x >= toggleTransform.position.x != (inverted ? !Toggle.isOn : Toggle.isOn))
                {
                    Toggle.isOn = !Toggle.isOn;
                }
                yield return null;
            }
            inputCoroutine = null;
        }
        
        void StopInputCoroutine()
        {
            if (inputCoroutine != null)
            {
                StopCoroutine(inputCoroutine);
                inputCoroutine = null;
            }
        }

        void MoveToggle(bool value, bool useAnimation)
        {
            var posValue = value;
            if (inverted)
                posValue = !posValue;

            var position = posValue ? .95f : .05f;

            var moveTween = MoveSequence(position).SetEase(Ease.InCubic).OnComplete(() => MoveComplete(value)).Play();
            var scaleTween = ScaleSequence();

            if (!useAnimation)
            {
                moveTween.Complete(true);
                scaleTween.Complete(true);
            }
            else
            {
                moveTween.Play();
                scaleTween.Play();
            }
        }

        void MoveComplete(bool value)
        {
            handle.GetChild(0).gameObject.SetActive(value);
            handle.GetChild(1).gameObject.SetActive(!value);
        }

        Sequence MoveSequence(float endValue)
        {
            var sequence = DOTween.Sequence();
            sequence.Insert(0f, handle.DOAnchorX(endValue, handleMoveDuration));
            sequence.SetUpdate(true);
            return sequence;
        }

        Sequence ScaleSequence()
        {
            var sequence = DOTween.Sequence();
            sequence.Append(handle.DOScale(.7f, handleMoveDuration * .15f));
            sequence.Append(handle.DOScale(.7f, handleMoveDuration * .7f));
            sequence.Append(handle.DOScale(1f, handleMoveDuration * .15f));
            sequence.SetUpdate(true);
            return sequence;
        }
    }
}