using JetBrains.Annotations;
using Zenject;

namespace UnityUtils.HZenject
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    public class HInjectAttribute : InjectAttribute { }
}