using Zenject;

namespace UnityUtils.HZenject
{
    public class MonoSignalBusInstaller : MonoInstaller<MonoSignalBusInstaller>
    {
        public override void InstallBindings()
        {
            if (Container.HasBinding<SignalBus>() == false)
                SignalBusInstaller.Install(Container);
        }
    }
}