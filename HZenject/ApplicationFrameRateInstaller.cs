using UnityEngine;
using Zenject;

namespace UnityUtils.HZenject
{
    public class ApplicationFrameRateInstaller : MonoInstaller<ApplicationFrameRateInstaller>
    {
        [SerializeField] int targetFrameRate = 60;
        
        public override void InstallBindings()
        {
            Application.targetFrameRate = targetFrameRate;
        }
    }
}