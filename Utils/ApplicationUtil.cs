﻿using UnityEngine;

namespace UnityUtils
{
    public static class ApplicationUtil
    {
        public static RuntimePlatform Platform
        {
            get
            {
#if UNITY_ANDROID
                return RuntimePlatform.Android;
#elif UNITY_IOS
                return RuntimePlatform.IPhonePlayer;
#elif UNITY_STANDALONE_OSX
                return RuntimePlatform.OSXPlayer;
#elif UNITY_STANDALONE_WIN
                return RuntimePlatform.WindowsPlayer;
#else
                return Application.platform; 
#endif
            }
        }

        public static bool IsProduction
        {
            get
            {
#if PRODUCTION
                return true;
#else
                return false;
#endif
            }
        }
    }
}