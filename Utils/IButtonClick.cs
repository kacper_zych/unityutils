using UnityEngine.Events;

namespace UnityUtils 
{
    public interface IButtonClick
    {
        event UnityAction OnClick;
    }
}