﻿using UnityEngine;

namespace UnityUtils
{
    public static class HInput
    {
        public static bool IsInputPressed()
        {
            if (IsOnMobilePlatform())
                return Input.touches.Length > 0;
            
            return Input.GetMouseButton(0);
        }

        public static bool IsInputDown()
        {
            if (IsOnMobilePlatform())
                return Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Began;
                
            return Input.GetMouseButtonDown(0);
        }

        public static Vector2 GetInputPosition()
        {
            if (IsOnMobilePlatform())
                return Input.touches.Length == 0 ? Vector2.zero : Input.touches[0].position;
                
            return Input.mousePosition;
        }

        static bool IsOnMobilePlatform()
        {
            return (ApplicationUtil.Platform == RuntimePlatform.Android ||
                    ApplicationUtil.Platform == RuntimePlatform.IPhonePlayer) 
                   && !Application.isEditor;
        }

        /// <returns> true if mouse or first touch is over any event system object ( usually gui elements ) </returns>
        public static bool IsPointerOverGameObject()
        {
            if (UnityEngine.EventSystems.EventSystem.current == null)
            {
                Debug.LogWarning("Event system is null. You need to add event system to your scene.");
                return false;
            }

            // check mouse
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                return true;

            // check touch
            if (Input.touchCount > 0)
            {
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                    return true;
            }

            return false;
        }
    }
}