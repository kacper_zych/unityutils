using System;

namespace UnityUtils
{
    public static class DateTimeUtils
    {
        static DateTime Epoch
        {
            get { return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); }
        }

        public static DateTime FromTimestamp(long timestamp)
        {
            return Epoch.AddSeconds(timestamp);
        }
        
        public static long ToTimestamp(this DateTime dateTime)
        {
            return dateTime.TotalSeconds();
        }
        
        static long TotalSeconds(this DateTime dateTime)
        {
            return (long) (dateTime - Epoch).TotalSeconds;
        }
    }
}