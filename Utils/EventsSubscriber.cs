using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityUtils
{
    public class EventsSubscriber
    {
        readonly List<EventSubscription> subscriptions = new List<EventSubscription>();

        public EventSubscription Add(Action subscribe, Action unsubscribe, params object[] nullChecks)
        {
            var eventSubscription = new EventSubscription(subscribe, unsubscribe, nullChecks);
            subscriptions.Add(eventSubscription);
            return eventSubscription;
        }

        public void Remove(EventSubscription subscription)
        {
            subscriptions.Remove(subscription);
        }

        public void Subscribe()
        {
            subscriptions.ForEach(x => x.TrySubscribe());
        }
        
        public void Unsubscribe()
        {
            subscriptions.ForEach(x => x.TryUnsubscribe());
        }

        public void UnsubscribeAndClear()
        {
            Unsubscribe();
            subscriptions.Clear();
        }

        public class EventSubscription
        {
            readonly Action subscribe;
            readonly Action unsubscribe;
            readonly object[] nullChecks;

            public EventSubscription(Action subscribe, Action unsubscribe, object[] nullChecks)
            {
                this.subscribe = subscribe;
                this.unsubscribe = unsubscribe;
                this.nullChecks = nullChecks;
            }

            public bool IsNotNull()
            {
                return nullChecks.All(x => x != null);
            }

            public void TrySubscribe()
            {
                if (IsNotNull())
                    subscribe?.Invoke();
            }
            
            public void TryUnsubscribe()
            {
                if (IsNotNull())
                    unsubscribe?.Invoke();
            }
        }
    }
}