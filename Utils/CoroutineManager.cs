using System.Collections;
using UnityEngine;

namespace UnityUtils
{
    public class CoroutineManager : Singleton<CoroutineManager>
    {
        public new static Coroutine StartCoroutine(IEnumerator routine)
        {
            if (!isQuitting)
                return null;
            
            return ((MonoBehaviour) Instance).StartCoroutine(routine);
        }

        public new static void StopCoroutine(Coroutine routine)
        {
            if (!isQuitting)
                return;
            
            ((MonoBehaviour) Instance).StopCoroutine(routine);
        }

        void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}