using UnityEngine;
using UnityEngine.Events;

namespace UnityUtils
{
    public class ColliderButton : MonoBehaviour, IButtonClick
    {
        [SerializeField] float overScale = 1.05f;
        [SerializeField] float clickScale = 0.95f;

        Vector3 originalScale;
        bool isScaled;
        
        public event UnityAction OnClick;

        public virtual void Awake()
        {
            originalScale = transform.localScale;
        }

        public virtual void OnMouseEnter()
        {
            if (HInput.IsPointerOverGameObject())
                return;
            
            Scale(overScale);
        }

        public virtual void OnMouseDown()
        {
            if (HInput.IsPointerOverGameObject())
                return;
            
            Scale(clickScale);
        }

        public virtual void OnMouseExit()
        {
            ResetScale();
        }

        void Scale(float scaleValue)
        {
            transform.localScale = originalScale * scaleValue;
            isScaled = true;
        }

        void OnMouseUpAsButton()
        {
            if (HInput.IsPointerOverGameObject())
                return;
            
            OnClick?.Invoke();
        }

        public virtual void OnDisable()
        {
            ResetScale();
        }

        void ResetScale()
        {
            if (isScaled)
                transform.localScale = originalScale;
        }
    }
}