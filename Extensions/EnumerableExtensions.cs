﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace UnityUtils.Extensions
{
    public static class EnumerableExtensions
    {
        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            Debug.Assert(source != null, nameof(source) + " != null");

            if (!source.Any())
            {
                throw new ArgumentException("Enumerable cannot be empty");
            }

            return source.ElementAt(UnityEngine.Random.Range(0, source.Count()));
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }
    }
}