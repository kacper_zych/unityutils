using System;
using System.Globalization;

namespace UnityUtils.Extensions
{
    public class UnitFormatter : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            else
                return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
                return null;

            int argValue = (int) arg;
            string argString = arg.ToString();

            if (argString.Length <= 5)
                return argValue.ToString("#0", CultureInfo.InvariantCulture);
            if (argString.Length <= 6)
                return argValue.ToString("#,.#0k", CultureInfo.InvariantCulture);
            if (argString.Length <= 9)
                return argValue.ToString("#,,.#0M", CultureInfo.InvariantCulture);
            if (argString.Length > 9)
                return argValue.ToString("#,,,.#0B", CultureInfo.InvariantCulture);


            return argString;
        }
    }
}