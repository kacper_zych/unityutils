using UnityEngine;

namespace UnityUtils.Extensions
{
    public static class GameObjectExtensions
    {
        public static bool IsDestroyed(this object obj)
        {
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return obj == null && !ReferenceEquals(obj, null);
        }

        public static void Deactivate<T>(this T mb) where T : MonoBehaviour
        {
            mb.gameObject.Deactivate();
        }

        public static void Deactivate(this GameObject go)
        {
            if (go == null || go.IsDestroyed())
            {
                return;
            }

            if (go.activeSelf)
            {
                go.SetActive(false);
            }
        }

        public static void Activate<T>(this T mb) where T : MonoBehaviour
        {
            mb.gameObject.Activate();
        }

        public static void Activate(this GameObject go)
        {
            if (go == null || go.IsDestroyed())
            {
                return;
            }

            if (go.activeSelf == false)
            {
                go.SetActive(true);
            }
        }

        public static void ChangeActive<T>(this T mb, bool activate) where T : MonoBehaviour
        {
            mb.gameObject.ChangeActive(activate);
        }

        public static void ChangeActive(this GameObject go, bool activate)
        {
            if (go == null || go.IsDestroyed())
            {
                return;
            }

            if (activate)
            {
                go.Activate();
            }
            else
            {
                go.Deactivate();
            }
        }

        public static RectTransform GetRectTransform(this GameObject c)
        {
            return c.transform as RectTransform;
        }

        public static T GetOrAddComponent<T>(this GameObject child) where T : Component
        {
            T result = child.GetComponent<T>();
            if (result == null)
            {
                result = child.gameObject.AddComponent<T>();
            }

            return result;
        }

        public static T GetComponentInParentEvenHidden<T>(this GameObject gameObject) where T : UnityEngine.Object
        {
            Transform parent = gameObject.transform;
            T componet;
            while (true)
            {
                if (parent == null)
                    break;
                componet = parent.GetComponent<T>();
                if (componet != null)
                    return componet;
                else
                    parent = parent.parent;
            }

            return null;
        }

        public static bool IsEqual<T1, T2>(this T1 mb1, T2 mb2) where T1 : MonoBehaviour where T2 : MonoBehaviour
        {
            return IsEqual(mb1?.gameObject, mb2?.gameObject);
        }

        public static bool IsEqual<T>(this T mb, GameObject go) where T : MonoBehaviour
        {
            return IsEqual(mb?.gameObject, go);
        }

        public static bool IsEqual(this GameObject go1, GameObject go2)
        {
            return go1?.GetInstanceID() == go2?.GetInstanceID();
        }
    }
}