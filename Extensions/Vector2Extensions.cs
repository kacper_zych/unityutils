using UnityEngine;

namespace UnityUtils.Extensions
{
    public static class Vector2Extensions
    {
        public static Vector2 Change(this Vector2 org, float? x = null, float? y = null)
        {
            return new Vector3(x ?? org.x, y ?? org.y);
        }

        public static Vector2 Move(this Vector2 org, float? x = null, float? y = null)
        {
            return org + Vector2.zero.Change(x, y);
        }
    }
}