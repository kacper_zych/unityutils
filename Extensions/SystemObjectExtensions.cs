namespace UnityUtils.Extensions
{
    public static class SystemObjectExtensions
    {
        public static bool IsNull(this System.Object systemObject)
        {
            return systemObject == null || systemObject.Equals(null);
        }

        public static bool IsNotNull(this System.Object systemObject)
        {
            return systemObject.IsNull() == false;
        }
    }
}