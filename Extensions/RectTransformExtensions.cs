using DG.Tweening;
using UnityEngine;

namespace UnityUtils.Extensions
{
    public static class RectTransformExtensions
    {
        public static Tweener DOAnchorX(this RectTransform target, float endValue, float duration, bool snapping = false)
        {
            return DOTween.To(() => target.anchorMin.x, value =>
            {
                target.anchorMin = target.anchorMin.Change(x: value);
                target.anchorMax = target.anchorMax.Change(x: value);
            }, endValue, duration).SetOptions(snapping).SetTarget(target);
        }
        
        public static Tweener DOAnchorY(this RectTransform target, float endValue, float duration, bool snapping = false)
        {
            return DOTween.To(() => target.anchorMin.y, value =>
            {
                target.anchorMin = target.anchorMin.Change(y: value);
                target.anchorMax = target.anchorMax.Change(y: value);
            }, endValue, duration).SetOptions(snapping).SetTarget(target);
        }

        /// <summary>Tweens a RectTransform's anchorMax to the given value.
        /// Also stores the RectTransform as the tween's target so it can be used for filtered operations</summary>
        /// <param name="target">Source RectTransform</param>
        /// <param name="endValue">The end value to reach</param>
        /// <param name="duration">The duration of the tween</param>
        /// <param name="snapping">If TRUE the tween will smoothly snap all values to integers</param>
        public static Tweener DOAnchorMax(
            this RectTransform target,
            Vector2 endValue,
            float duration,
            bool snapping = false)
        {
            return DOTween.To(() => target.anchorMax, value => target.anchorMax = value, endValue, duration).SetOptions(snapping).SetTarget(target);
        }

        /// <summary>Tweens a RectTransform's anchorMin to the given value.
        /// Also stores the RectTransform as the tween's target so it can be used for filtered operations</summary>
        /// <param name="target">Source RectTransform</param>
        /// <param name="endValue">The end value to reach</param>
        /// <param name="duration">The duration of the tween</param>
        /// <param name="snapping">If TRUE the tween will smoothly snap all values to integers</param>
        public static Tweener DOAnchorMin(
            this RectTransform target,
            Vector2 endValue,
            float duration,
            bool snapping = false)
        {
            return DOTween.To(() => target.anchorMin,  value => target.anchorMin = value, endValue, duration).SetOptions(snapping).SetTarget(target);
        }
    }
}