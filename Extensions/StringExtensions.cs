using System;

namespace UnityUtils.Extensions
{
    public static class StringExtensions
    {
        public static string[] Split(this string s, string delimiter)
        {
            return s.Split(new[] {delimiter}, StringSplitOptions.None);
        }

        public static int WordCount(this String str)
        {
            return str.Split(new[] {' ', '.', '?'}, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool NotEmpty(this string str)
        {
            return string.IsNullOrEmpty(str) == false;
        }

        public static string Format(this string str, params object[] args)
        {
            return string.Format(str, args);
        }

        public static bool ContainsIgnoreCase(this string source, string toCheck)
        {
            var sourceLowerCase = source.ToLower();
            var toCheckLowerCase = toCheck.ToLower();
            return sourceLowerCase.Contains(toCheckLowerCase);
        }
    }
}