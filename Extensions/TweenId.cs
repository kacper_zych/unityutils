using UnityEngine;

namespace UnityUtils.Extensions
{
    public static class TweenId
    {
        const string SHOW = "SHOW_";
        const string HIDE = "HIDE_";

        public static string GetShowId(GameObject gameObject)
        {
            return GetId(SHOW, gameObject.GetInstanceID());
        }

        public static string GetHideId(GameObject gameObject)
        {
            return GetId(HIDE, gameObject.GetInstanceID());
        }

        public static string GetId(string prefix, int instanceId)
        {
            return prefix + instanceId;
        }
    }
}